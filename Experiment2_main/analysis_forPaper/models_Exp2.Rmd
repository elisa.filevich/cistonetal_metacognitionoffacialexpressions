---
title: "Faces Analysis - Run linear models for data from Experiment 2"
author: "EF"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    toc: true
    toc_float:
      collapsed: false
      smooth_scroll: false
code_folding: show
number_sections: true
---

Notes
1. Useful plotting tutorial here https://cran.r-project.org/web/packages/tidybayes/vignettes/tidy-rstanarm.html

```{r setup, include=FALSE}
rm(list =ls())
library(tidyverse)
library(reshape2)
library(ggplot2)
library(here)
library(patchwork)
library(bayestestR)
library(brms)
library(tidybayes)
library(ggsci)

colorblind.palette.2 <- c("#000000", "#E69F00", "#56B4E9", "#009E73",
          #"#F0E442", #remove the very light one
          "#0072B2", "#D55E00", "#CC79A7")

colorblind.palette.2 <- pal_npg("nrc")(7)

```


## Load and curate data 
This includes summary landmark distances obtained from preprocessed data (procrustes aligned) from python, and all judgments
```{r read_data}
facesData <- read.csv(here('results_ML.csv'))
facesData$subject <- as.factor(facesData$subject)

#At preregistration we said we would use a cutoff that was specific for each participant, because some people tend to laugh or take a very long time to reproduce the expressions. So we so a visual inspection
ggplot(facesData, aes(x = RT_takePic))+
  geom_histogram() + 
  facet_wrap(~subject)

#From visual inspection, it looks like 12 seconds would be reasonable, but we'll do a more principled approach
# Exclude trials that are above the 95% of the distribution for each participant 
excludingThreshold <- facesData %>% group_by(subject) %>% 
  summarize(percentile95 = quantile(RT_takePic, 0.95) ) %>% 
  select(subject, percentile95)

shortTrials <- facesData %>% group_by(subject) %>% 
  summarize(shortTrials = sum(RT_takePic < 0.3)) %>%  #as preregistered
  select(subject, shortTrials) %>% 
  print(n=40)


#Report range of exclusion threshold for RTs
mean(excludingThreshold$percentile95) 
range(excludingThreshold$percentile95)

facesData.clean <- facesData %>% group_by(subject) %>% 
  filter(RT_takePic < quantile(RT_takePic, 0.95) ) %>% 
  filter(RT_takePic > 0.3) 

#Check that this worked
ggplot(facesData.clean, aes(x = RT_takePic))+
  geom_histogram() + 
  facet_wrap(~subject)

facesData <- facesData.clean 

#Check if distance is normally distributed. 
ggplot(facesData, aes(x = distance_RSSQ))+
  geom_histogram() + 
  facet_wrap(~subject)

#Sort of, but it's skewed leftwards and some have long tails

# Add the log-transformed 
facesData$log_distance_RSSQ <- log(facesData$distance_RSSQ)

ggplot(facesData, aes(x = log_distance_RSSQ))+
  geom_histogram() + 
  facet_wrap(~subject)

# Plot confidence distributions for all subjects
ggplot(facesData, aes(x = confidence))+
  geom_histogram() + 
  facet_wrap(~subject)

```

There's no obvious need to exclude any subjects based on the distributions of confidence ratings


# Run mixed-effects models 
## Set priors (et al) for all models
```{r setPriors}

# The prior for the slope is based on the ratio-of-scales heuristic. In this case, the range of distances was range(facesData$log_distance_RSSQ) = 4.878056 7.821554 ~= 3 a.u.
#On the other hand the maximum possible confidence difference is 1. 
# So I use a normal prior centered on 0 with an SD=1/3.
# For the intercept, a perfect subject should have rating = 1 at 0 distance. So I centered the prior at 0.8 with an SD = 0.5, which corresponds to the prior centered at 4/5, SD =3 that we used for experiment 1, following (the gist of) the room-to-move heuristic.  
heuristicPriors.full <- prior("normal(0,1.0/3)", class = "b", coef = "log_distance_RSSQ") + 
  prior(normal(0.8, 0.5), class = "Intercept")
heuristicPriors.null <- prior(normal(0.8, 0.5), class = "Intercept")

# Also following the ratio-of-scales, the correspondence between the two ratings is traightforward because they are both on the same scale so SD = 1. The main difference to the priors above is that now we expect the perfect intercept at 0, and not 1.  
identityPrior.full <- prior("normal(0,1)", class = "b", coef = "postHocRating") + 
  prior(normal(0, 0.5), class = "Intercept")                 
identityPrior.null <- prior(normal(0, 0.5), class = "Intercept")

# RT priors. 
# Prior for the slope taken from the Filevich, Koß and Faivre, eNeuro 2020
heuristicPriors.RT.full <- prior("normal(0,0.5)", class = "b", coef = "RT_takePic") + 
  prior(normal(0.8, 0.5), class = "Intercept")
heuristicPriors.RT.null <- prior(normal(0.8, 0.5), class = "Intercept")


iterations =  15000 
warmup = 5000


```


## 1: Do participants' ratings track the 2D distance (confidence vs distance)
### Plot to examine the data
```{r confidenceVsDistance_data, warning=FALSE}

FigS1 <- ggplot(facesData, aes(x=log_distance_RSSQ, y=confidence)) +
  geom_point(alpha=0.5) +
  geom_smooth(method=lm, colour=colorblind.palette.2[2]) +
  xlim(0,7) +
  scale_x_continuous(name="Log distance (a.u)") +
  scale_y_continuous(name="Confidence rating", limits=c(0, 1)) +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) + 
  facet_wrap(~subject, nrow = 8) +
  coord_cartesian(ylim=c(0,1))

FigS1

ggsave(here("../../Figures/FigureS1.png"), 
       #device = cairo_pdf, #only if pdf
       plot = FigS1, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 297, 
       units = "mm")

```

### Run a bayesian linear mixed regression
```{r confidenceVsDistance_model, warning=FALSE, message = FALSE}
# Full model
SubjConf_distanceMeasures.full <- brm(confidence ~ log_distance_RSSQ + 
                                        (1+ log_distance_RSSQ |subject) + (1 |expression),
                                      data = facesData, family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.full,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = "brms_models/SubjConf_distanceMeasures.full")
summary(SubjConf_distanceMeasures.full)
bayes_R2(SubjConf_distanceMeasures.full)

plot(SubjConf_distanceMeasures.full, ask = FALSE, newpage = FALSE) 

# Null model
SubjConf_distanceMeasures.null <- brm(confidence ~ 1 + 
                                        (1+ log_distance_RSSQ |subject) + (1 |expression),
                                      data = facesData, family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.null,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = "brms_models/SubjConf_distanceMeasures.null")

plot(SubjConf_distanceMeasures.null, ask = FALSE, newpage = FALSE) 


```

I run the same model as above again, because I want the subjIDs to join these data with the other covariates (TAS, visual metacog) unequivocally by ID. 
```{r addendum_rerunModels_subjID}

SubjConf_distanceMeasures_subjID.full <- brm(confidence ~ log_distance_RSSQ + 
                                        (1+ log_distance_RSSQ |subjID) + (1 |expression),
                                      data = facesData, family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.full,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = "brms_models/SubjConf_distanceMeasures_subjID.full")

similarity_distanceMeasures_subjID.full = brm(postHocRating ~ log_distance_RSSQ + 
                                         (1+ log_distance_RSSQ |subjID) + (1 |expression),
                                       data = facesData, family = gaussian(),
                                       cores = 4, chains = 4,
                                       prior = heuristicPriors.full,
                                       iter = iterations, warmup = warmup,
                                       save_all_pars = TRUE,
                                       sample_prior = TRUE,
                                       file = "brms_models/similarity_distanceMeasures_subjID.full")


```


### Plot model predictions
```{r confidenceVsDistance_modelPlot, warning=FALSE, message = FALSE}
#conditional_effects gives you the basic plot. Using plot() and [[1]] allows you to cusomize it in ggplot. There's one element in an ordered list per effect
fig2.A <- plot(conditional_effects(SubjConf_distanceMeasures.full, "log_distance_RSSQ"))[[1]] +
  labs(x = "Log distance (a.u.)", y = "Confidence") +
  ggtitle("A. Model estimates") +
  geom_smooth(colour = "black") +
  coord_cartesian(ylim = c(0,1)) 

fig2.A 


```


```{r extractIndividualEffects, include=FALSE}
#Extract the random effects for each participant, to then correlate with visual metacog and TAS.
#I excluded this from the code for now because I re-extract in a different script (correlations_faces_visual_TAS.Rmd)
participantSlopes_confVsDistance <- SubjConf_distanceMeasures.full %>%
  spread_draws(b_log_distance_RSSQ, r_subject[subject,]) %>% 
  mutate(effect_estimate = b_log_distance_RSSQ + r_subject) %>%
  mutate(subject = as.character(subject)) %>% 
  group_by(subject) %>% 
  summarise(subjectSlope = mean(effect_estimate)) %>% print(n=40)
#save("participantSlopes_confVsDistance", file = here('participantSlopes_confVsDistance.Rda'))

```


### Compare against the null model
```{r confidenceVsDistance_modelComparison, warning=FALSE, message = FALSE}

BF_comparison <- bayestestR::bayesfactor_models(SubjConf_distanceMeasures.full, denominator = SubjConf_distanceMeasures.null)
BF_comparison

# We use rope_range to follow Kruschke (2018), who suggests that the region of practical equivalence could be set, by default, to a range from -0.1 to 0.1 of a standardized parameter (negligible effect size according to Cohen, 1988). 
ROPE <- rope_range(SubjConf_distanceMeasures.full)
ROPE

# Plot the whole group posterior samples
Fig2.B.groupPosterior <- SubjConf_distanceMeasures.full %>%
  spread_draws(b_log_distance_RSSQ) %>% 
  mutate(effect_estimate = b_log_distance_RSSQ) %>%
 ggplot(aes(x = effect_estimate, fill = stat(ROPE[[1]] < x & x < ROPE[[2]]))) + 
  stat_halfeye(.width = c(.95)) +
  geom_vline(xintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  theme(legend.position = "none") +
  ggtitle("B. Fixed effect") +
  labs(x = "Effect of distance", y = "Probability density") +
  coord_cartesian(xlim=c(-0.20,0.05))


#tidybayes::get_variables(SubjConf_distanceMeasures.full)
Fig2.C.randomSlopes <- SubjConf_distanceMeasures.full %>%
  spread_draws(b_log_distance_RSSQ, r_subject[subject,]) %>%
  ungroup() %>% 
  mutate(effect_estimate = b_log_distance_RSSQ + r_subject) %>% 
  #mutate(subject = as.character(subject)) %>% 
  mutate(subject = as.factor(subject)) %>% 
  mutate(subject = forcats::fct_reorder(subject, effect_estimate, mean)) %>%
  ggplot(aes(x = subject, y = effect_estimate, fill = stat(ROPE[[1]] < y & y < ROPE[[2]]))) +
  stat_slab() +
  geom_hline(yintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  theme(axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        legend.position = "none") +
  ggtitle("C. Random slopes") +
  labs(y = "Effect of distance", x = "Participant") +
  coord_cartesian(ylim=c(-0.2,0.15))
Fig2.C.randomSlopes

```


```{r makeFig2}

Fig2 <- (fig2.A + Fig2.B.groupPosterior) / Fig2.C.randomSlopes +
  plot_layout(widths = c(1,2)) +
  plot_annotation(
    title = 'Confidence ratings',
  )
Fig2

ggsave(here("../../Figures/Figure2.png"), 
       #device = cairo_pdf, #only if pdf
       plot = Fig2, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 120, 
       units = "mm")
 
```

## 2. Are the 2D-distances meaningful? (Each subject's Similarity rating vs distance)
### Plot to examine the data
```{r similarityRatingsVsDistance_data, warning=FALSE, message = FALSE}

FigS2 <- ggplot(facesData, aes(x=log_distance_RSSQ, y=postHocRating)) +
  geom_point(alpha=0.5) +
  geom_smooth(method=lm, colour=colorblind.palette.2[3]) +
  xlim(0,7) +
  scale_x_continuous(name="Log distance (a.u)") +
  scale_y_continuous(name="Similarity rating") +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) + 
  facet_wrap(~subject, nrow = 8) +
  coord_cartesian(ylim=c(0,1))

FigS2

ggsave(here("../../Figures/FigureS2.png"), 
       #device = cairo_pdf, #only if pdf
       plot = FigS2, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 297, 
       units = "mm")

```


### Run a bayesian linear mixed regression
```{r similarityRatingsVsDistance_model, warning=FALSE, message = FALSE}

similarity_distanceMeasures.full = brm(postHocRating ~ log_distance_RSSQ + 
                                         (1+ log_distance_RSSQ |subject) + (1 |expression),
                                       data = facesData, family = gaussian(),
                                       cores = 4, chains = 4,
                                       prior = heuristicPriors.full,
                                       iter = iterations, warmup = warmup,
                                       save_all_pars = TRUE,
                                       sample_prior = TRUE,
                                       file = "brms_models/similarity_distanceMeasures.full")
summary(similarity_distanceMeasures.full)
bayes_R2(similarity_distanceMeasures.full)

plot(similarity_distanceMeasures.full, ask=FALSE, newpage=FALSE)


similarity_distanceMeasures.null = brm(postHocRating ~ 1 + 
                                         (1+ log_distance_RSSQ |subject) + (1 |expression),
                                       data = facesData, family = gaussian(),
                                       cores = 4, chains = 1,
                                       prior = heuristicPriors.null,
                                       iter = iterations, warmup = warmup,
                                       save_all_pars = TRUE,
                                       sample_prior = TRUE,
                                       file = "brms_models/similarity_distanceMeasures.null")
summary(similarity_distanceMeasures.null)
plot(similarity_distanceMeasures.null, ask=FALSE, newpage=FALSE)

```

### Plot linear model predictions
```{r similarityRatingsVsDistance_modelPlot, warning=FALSE, message = FALSE}

fig3.A <- plot(conditional_effects(similarity_distanceMeasures.full, "log_distance_RSSQ"))[[1]] +
  labs( x = "Log distance (a.u.)", y = "Rating") +
  ggtitle("A. Model estimates") +
  geom_smooth(colour = "black") +
  coord_cartesian(ylim = c(0,1)) 

fig3.A

```

### Compare against the null model
```{r similarityRatingsVsDistance_modelComparison, warning=FALSE, message = FALSE}

BF_comparison <- bayestestR::bayesfactor_models(similarity_distanceMeasures.full, denominator = similarity_distanceMeasures.null)
BF_comparison


ROPE <- rope_range(similarity_distanceMeasures.full)

# Plot the whole group posterior samples
Fig3.B.groupPosterior <- similarity_distanceMeasures.full %>%
  spread_draws(b_log_distance_RSSQ) %>% 
  mutate(effect_estimate = b_log_distance_RSSQ) %>%
 ggplot(aes(x = effect_estimate, fill = stat(ROPE[[1]] < x & x < ROPE[[2]]))) + 
  stat_halfeye(.width = c(.95)) +
  geom_vline(xintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  theme(legend.position = "none") +
  ggtitle("B. Fixed effect") +
  labs(x = "Effect of distance", y = "Probability density") +
  coord_cartesian(xlim=c(-0.20,0.05))



Fig3.C.randomSlopes <- similarity_distanceMeasures.full %>%
  tidybayes::spread_draws(b_log_distance_RSSQ, r_subject[subject,]) %>%
  ungroup() %>%
  mutate(slope = b_log_distance_RSSQ + r_subject) %>%
  mutate(subject = as.factor(subject)) %>% 
  mutate(subject = forcats::fct_reorder(subject, slope, mean)) %>%
  ggplot(aes(x = subject, y = slope, fill = stat(ROPE[[1]] < y & y < ROPE[[2]]))) +
  stat_slab() +
  geom_hline(yintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  labs(y = "Effect of distance", x = "Participant") +
  theme(axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        legend.position = "none") +
   ggtitle("C. Random slopes") + 
  coord_cartesian(ylim=c(-0.3,0.1))
Fig3.C.randomSlopes
```

Conclusion: distance of all the face is predicted accurately by the 2D distance. That means that the 2D (all the face) distance is meaningful. Additionally, here the ratings were from the same participants. So it cannot be argued that participants were bad at rating similarity, in general. 



```{r makeFig3}

Fig3 <- (fig3.A + Fig3.B.groupPosterior) /Fig3.C.randomSlopes + 
  plot_layout(widths = c(1,2)) +
  plot_annotation(
    title = 'Similarity ratings',
  )
Fig3


ggsave(here("../../Figures/Figure3.png"), 
       #device = cairo_pdf, #only if pdf
       plot = Fig3, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 120, 
       units = "mm")
 
```


## 3. [Exploratory] What about the other connection? (Subject's Offline rating vs Online rating)
### Quick-and-dirty plot
```{r confidenceVsSimilarityRating_data, warning=FALSE, message = FALSE}

FigS3 <- ggplot(facesData, aes(x=postHocRating, y=confidence)) +
  geom_point(alpha=0.5, shape=16) +
  geom_smooth(method=lm, colour=colorblind.palette.2[4]) +
  xlim(0,7) +
  scale_x_continuous(name="Similarity rating") +
  scale_y_continuous(name="Confidence rating") +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) + 
  facet_wrap(~subject, nrow=8) +
  coord_cartesian(ylim=c(0,1))

FigS3


ggsave(here("../../Figures/FigureS3.png"), 
       #device = cairo_pdf, #only if pdf
       plot = FigS3, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 297, 
       units = "mm")

```



### Run bayesian linear mixed regression
Here we change the random structure of the model relative to the other ones we ran before. We no longer incorporate the actual distance as a random slope. This is because we just want to know how well the two measures go together. We do include random intercepts  
```{r confidenceVsSimilarity_model, warning=FALSE, message = FALSE}

# Remove trials where confidence (123 trials) and similarity (5 trials) are missing. This was not a problem in other models but here the null and full model otherwise differ on the number of observations (hence data), and are therefore not comparable with a bayes factor. This is because the null model does not include similarity ratings at all. 
# which(!complete.cases(facesData$postHocRating))
# which(!complete.cases(facesData$confidence))
facesData.onlyComplete <- facesData %>% na.omit

confidence_similarity.full = brm(confidence ~ postHocRating + 
                                   (1 |subject) + (1 |expression),
                                 data = facesData.onlyComplete, family = gaussian(),
                                 cores = 4, chains = 4,
                                 prior = identityPrior.full, 
                                 iter = iterations, warmup = warmup,
                                 save_all_pars = TRUE,
                                 sample_prior = TRUE,
                                 file = "brms_models/confidence_similarity.full")
summary(confidence_similarity.full)
bayes_R2(confidence_similarity.full)

plot(confidence_similarity.full, ask=FALSE, newpage=FALSE)


confidence_similarity.null = brm(confidence ~ 1 + 
                                   (1 |subject) + (1 |expression),
                                 data = facesData.onlyComplete, family = gaussian(),
                                 cores = 4, chains = 4,
                                 prior = identityPrior.null, 
                                 iter = iterations, warmup = warmup,
                                 save_all_pars = TRUE,
                                 sample_prior = TRUE,
                                 file = "brms_models/confidence_similarity.null")
summary(confidence_similarity.null)
plot(confidence_similarity.null, ask=FALSE, newpage=FALSE)

```

### Plot linear model predictions
```{r confidenceVsSimilarity_modelPlot, warning=FALSE, message = FALSE}

fig5.A <- plot(conditional_effects(confidence_similarity.full, "postHocRating"))[[1]] +
  labs( x = "Confidence Rating", y = "Similarity Rating") +
  ggtitle("A. Model estimates") +
  geom_smooth(colour = "black") +
  coord_cartesian(ylim=c(0,1))

fig5.A

```


### Compare against the null model
```{r confidenceVsSimilarity_modelComparison, warning=FALSE, message = FALSE}

BF_comparison <- bayestestR::bayesfactor_models(confidence_similarity.full, denominator = confidence_similarity.null)
BF_comparison


ROPE <- rope_range(confidence_similarity.full)

# Plot the whole group posterior samples
Fig5.B.groupPosterior <- confidence_similarity.full %>%
  spread_draws(b_postHocRating) %>% 
  mutate(effect_estimate = b_postHocRating) %>%
 ggplot(aes(x = effect_estimate, fill = stat(ROPE[[1]] < x & x < ROPE[[2]]))) + 
  stat_halfeye(.width = c(.95)) +
  geom_vline(xintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  theme(legend.position = "none") +
  ggtitle("B. Fixed effect") +
  labs(x = "Effect of confidence", y = "Probability density") 


Fig5.C.randomSlopes <- confidence_similarity.full %>%
  tidybayes::spread_draws(b_postHocRating, r_subject[subject,]) %>%
  ungroup() %>%
  mutate(slope = b_postHocRating + r_subject) %>%
  mutate(subject = as.factor(subject)) %>% 
  mutate(subject = forcats::fct_reorder(subject, slope, mean)) %>%
  ggplot(aes(x = subject, y = slope, fill = stat(ROPE[[1]] < y & y < ROPE[[2]]))) +
  stat_slab() +
  geom_hline(yintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  labs(y = "Effect of confidence", x = "Participant") +
  theme(axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        legend.position = "none") +
   ggtitle("C. Random slopes")
Fig5.C.randomSlopes

```



```{r makeFig5}

Fig5 <- (fig5.A + Fig5.B.groupPosterior) / Fig5.C.randomSlopes + 
  plot_layout(widths = c(1, 2)) +
  plot_annotation(
    title = 'Similarity and confidence ratings',
  )
Fig5

ggsave(here("../../Figures/Figure5.png"), 
       #device = cairo_pdf, #only if pdf
       plot = Fig5, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 120, 
       units = "mm")
 
```

## Relationship between RT and confidence
```{r relationship_Conf.vs.RT}

confidence_RT.full = brm(confidence ~ RT_takePic + 
                           (RT_takePic |subject) + (1 |expression),
                         data = facesData, family = gaussian(),
                         cores = 4, chains = 4,
                         prior = heuristicPriors.RT.full, 
                         iter = iterations, warmup = warmup,
                         save_all_pars = TRUE,
                         sample_prior = TRUE,
                         file = "brms_models/confidence_RT.full"
                         )
summary(confidence_RT.full)
bayes_R2(confidence_RT.full)


confidence_RT.null = brm(confidence ~ 1 + 
                           (1 |subject) + (1 |expression),
                         data = facesData, family = gaussian(),
                         cores = 4, chains = 4,
                         prior = heuristicPriors.RT.null, 
                         iter = iterations, warmup = warmup,
                         save_all_pars = TRUE,
                         sample_prior = TRUE,
                         file = "brms_models/confidence_RT.null"
                         )
summary(confidence_RT.null)
bayes_R2(confidence_RT.null)

BF_comparison <- bayestestR::bayesfactor_models(confidence_RT.full, denominator = confidence_RT.null)
BF_comparison


# Also get individual r values for confidence ~ RT_takepic
# this should be one way
coef(confidence_RT.full)

# This is a simpler way, and more validly comparable to the other ML r values
r.confidence.RT.subjectwise <- facesData %>% 
  select(RT_takePic, subject, confidence) %>% 
  na.omit() %>% 
  group_by(subject) %>% 
  summarise(subj_r_conf_RT = cor(confidence, RT_takePic)) %>% 
  print(n=40)


write.csv(r.confidence.RT.subjectwise, file = here("ML_r_values","r.confidence.RT.subjectwise.csv")) 
```



### relationship between confidence and the adjusted distance
Measure the  (incorporates the weights obtained by ML for each subject and landmark) 
```{r confidenceVsMLDistance_model, warning=FALSE, message = FALSE}
#First plot the new weighted_distance, especially in relationship to the log_distance_RSSQ

# Weighted distance was calculated not using the log of anything, so it's not comparable to RSSQ. 
range(facesData$log_distance_RSSQ)
range(facesData$weighted_distance_sign)
# I divide this by 15 which should make them roughly match
facesData$weighted_distance_sign_scaled <- facesData$weighted_distance_sign/15 
range(facesData$weighted_distance_sign_scaled)

facesData.MLadjusted.plot <- facesData %>% select(subjID, subject,
                     distance_RSSQ, log_distance_RSSQ, expression,
                     confidence, weighted_distance_sign_scaled) %>% 
  melt(., id.vars = c("subjID", "subject", "expression", "confidence"), value.name = "distance") %>% 
  filter(variable %in% c("log_distance_RSSQ", "weighted_distance_sign_scaled")) %>% 
  ggplot(., aes(x = distance, fill = variable))+
  geom_histogram() + 
  facet_wrap(~subject)
facesData.MLadjusted.plot


# Get the range of the weight values
all.weights <- facesData %>% select(c("subjID", starts_with("wlm"))) 
all.weights.molten <- melt(all.weights,id.vars = "subjID", variable.name = "w")
# These commands should plot all weights. On my desktop computer it takes too long - not sure why
#ggplot(all.weights.molten, aes(x = value)) +
#  facet_wrap(~subjID) +
#  geom_density()
range(all.weights.molten$value)

```


### Run a bayesian linear mixed regression
```{r relationship_Conf.vs.adjustedDist}
heuristicPriors.MLdist_signed.full <- prior("normal(0,1.0/3)", class = "b", coef = "weighted_distance_sign_scaled") + 
  prior(normal(0.8, 0.5), class = "Intercept")
heuristicPriors.MLdist.null <- prior(normal(0.8, 0.5), class = "Intercept")

# Full model: weightedDistanceSign
SubjConf_weightedDistanceSign.full <- brm(confidence ~ weighted_distance_sign_scaled + 
                                        (1+ weighted_distance_sign_scaled |subject) + (1 |expression),
                                      data = facesData, family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.MLdist_signed.full,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = "brms_models/SubjConf_weightedDistanceSign.full")
summary(SubjConf_weightedDistanceSign.full)
bayes_R2(SubjConf_weightedDistanceSign.full)

#print the summary again with more decimals
print(SubjConf_weightedDistanceSign.full, digits = 10)


# Null model: weightedDistance_signed
SubjConf_weightedDistanceSign.null <- brm(confidence ~ 1 + 
                                        (1+ weighted_distance_sign_scaled |subject) + (1 |expression),
                                      data = facesData, family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.MLdist.null,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = "brms_models/SubjConf_weightedDistanceSign.null")
summary(SubjConf_weightedDistanceSign.null)


BF_comparison <- bayestestR::bayesfactor_models(SubjConf_weightedDistanceSign.full, denominator = SubjConf_weightedDistanceSign.null)
BF_comparison

```


### Plot model results
We don't include this figure in the paper, but it's good to have
```{r makeFig5bis}

ROPE <- rope_range(SubjConf_weightedDistanceSign.full)

fig5bis.A <- plot(conditional_effects(SubjConf_weightedDistanceSign.full, "weighted_distance_sign_scaled"))[[1]] +
  labs( x = "Weighted RSSQ", y = "Confidence Rating") +
  ggtitle("A. Model estimates") +
  geom_smooth(colour = "black") +
  coord_cartesian(ylim=c(0,1))

fig5bis.A

# Plot the whole group posterior samples
Fig5bis.B.groupPosterior <- SubjConf_weightedDistanceSign.full %>%
  spread_draws(b_weighted_distance_sign_scaled) %>% 
  mutate(effect_estimate = b_weighted_distance_sign_scaled) %>%
 ggplot(aes(x = effect_estimate, fill = stat(ROPE[[1]] < x & x < ROPE[[2]]))) + 
  stat_halfeye(.width = c(.95)) +
  geom_vline(xintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  theme(legend.position = "none") +
  ggtitle("B. Fixed effect") +
  labs(x = "Effect of ML-weighted distance", y = "Probability density") #+
  #coord_cartesian(xlim=c(-0.20,0.05))
Fig5bis.B.groupPosterior


Fig5bis.C.randomSlopes <- SubjConf_weightedDistanceSign.full %>%
  tidybayes::spread_draws(b_weighted_distance_sign_scaled, r_subject[subject,]) %>%
  ungroup() %>%
  mutate(slope = b_weighted_distance_sign_scaled + r_subject) %>%
  mutate(subject = as.factor(subject)) %>% 
  mutate(subject = forcats::fct_reorder(subject, slope, mean)) %>%
  ggplot(aes(x = subject, y = slope, fill = stat(ROPE[[1]] < y & y < ROPE[[2]]))) +
  stat_slab() +
  geom_hline(yintercept = c(-ROPE, ROPE), linetype = "dashed") +
  scale_fill_manual(values = c("gray80", "skyblue")) +
  labs(y = "Effect of weighted distance", x = "Participant") +
  theme(axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        legend.position = "none") +
   ggtitle("C. Random slopes")
Fig5bis.C.randomSlopes



Fig5bis <- (fig5bis.A + Fig5bis.B.groupPosterior) / Fig5bis.C.randomSlopes + 
  plot_layout(widths = c(1, 2)) +
  plot_annotation(
    title = 'confidence ratings and weighted distance',
  )
Fig5bis

ggsave(here("../../Figures/Fig5bis.png"), 
       #device = cairo_pdf, #only if pdf
       plot = Fig5bis, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 120, 
       units = "mm")
 

```

