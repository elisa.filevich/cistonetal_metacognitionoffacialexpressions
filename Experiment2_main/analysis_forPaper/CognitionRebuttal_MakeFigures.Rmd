---
title: "Faces Analysis - Create figures for rebuttal letter"
author: "EF"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
  toc: true
  toc_float:
    collapsed: false
smooth_scroll: false
code_folding: show
number_sections: true
---
  

```{r setup, include=FALSE}
rm(list =ls())
library(GGally) #for ggpairs for reviewer 3
library(tidyverse)
library(reshape2)
library(ggplot2)
library(here)
library(patchwork)
library(bayestestR)
library(BayesFactor)
library(brms)
library(tidybayes)
library(ggsci)
library(gridExtra) #for grid.arrange
library(boot) #get bootstrapped CI for ratio of two slopes

colorblind.palette.2 <- c("#000000", "#E69F00", "#56B4E9", "#009E73",
                          #"#F0E442", #remove the very light one
                          "#0072B2", "#D55E00", "#CC79A7")

colorblind.palette.2 <- pal_npg("nrc")(7)

```


## Load and curate data 
This includes summary landmark distances obtained from preprocessed data (procrustes aligned) from python, and all judgments
```{r read_data}
facesData <- read.csv(here('results_ML.csv'))
facesData$subject <- as.factor(facesData$subject)

# Exclude trials that are above the 95% of the distribution for each participant 
facesData.clean <- facesData %>% group_by(subject) %>% 
  filter(RT_takePic < quantile(RT_takePic, 0.95) ) %>% 
  filter(RT_takePic > 0.3) 

facesData <- facesData.clean 

# Add the log-transformed 
facesData$log_distance_RSSQ <- log(facesData$distance_RSSQ)
```


## 1: Do participants' ratings track the 2D distance (confidence vs distance)
### Plot to examine the data
```{r confidenceVsDistance_data, warning=FALSE}

participant_numbers <- as_labeller(
     c(`1` = "Participant 1", `2` = "Participant 2",`3` = "Participant 3", `4` = "Participant 4", 
       `5` = "Participant 5", `6` = "Participant 6",`7` = "Participant 7", `8` = "Participant 8", 
       `9` = "Participant 9", `10` = "Participant 10",`11` = "Participant 11", `12` = "Participant 12",
       `13` = "Participant 13", `14` = "Participant 14",`15` = "Participant 15", `16` = "Participant 16",
       `17` = "Participant 17", `18` = "Participant 18",`19` = "Participant 19", `20` = "Participant 20",
       `21` = "Participant 21", `22` = "Participant 22",`23` = "Participant 23", `24` = "Participant 24",
       `25` = "Participant 25", `26` = "Participant 26",`27` = "Participant 27", `28` = "Participant 28",
       `29` = "Participant 29", `30` = "Participant 30",`31` = "Participant 31", `32` = "Participant 32",
       `33` = "Participant 33", `34` = "Participant 34",`35` = "Participant 35", `36` = "Participant 36",
       `37` = "Participant 37", `38` = "Participant 38",`39` = "Participant 39", `40` = "Participant 40"))


FigR1 <- ggplot(facesData, aes(x=RT_takePic, y=confidence)) +
  geom_point(alpha=0.5) +
  geom_smooth(method=lm, colour=colorblind.palette.2[2]) +
  xlim(0,7) +
  scale_x_continuous(name="RT take picture (ms)") +
  scale_y_continuous(name="Confidence rating", limits=c(0, 1)) +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) + 
  facet_wrap(~subject, nrow = 8, labeller = participant_numbers) +
  coord_cartesian(ylim=c(0,1))

FigR1


ggsave(here("../../Figures/FigureR1.png"), 
       #device = cairo_pdf, #only if pdf
       plot = FigR1, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 297, 
       units = "mm")



FigR2 <- ggplot(facesData, aes(x=RT_takePic, y=log_distance_RSSQ)) +
  geom_point(alpha=0.5) +
  geom_smooth(method=lm, colour=colorblind.palette.2[2]) +
  xlim(0,7) +
  scale_x_continuous(name="RT take picture (ms)") +
  scale_y_continuous(name="Log distance (a.u)") +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) + 
  facet_wrap(~subject, nrow = 8, labeller = participant_numbers)
 

FigR2

ggsave(here("../../Figures/FigureR2.png"), 
       #device = cairo_pdf, #only if pdf
       plot = FigR2, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 297, 
       units = "mm")
```

```{r singleSubjSummary_rebuttal2, warning=FALSE}

facesData_summary <- facesData %>% 
  group_by(subject) %>% 
  summarise_at(vars(RT_takePic, confidence, log_distance_RSSQ), list(subjMean = mean)) %>% 
  rename_with(~ c("Mean_RT", "Mean_Confidence", "Mean_Distance"), c(RT_takePic_subjMean, confidence_subjMean, log_distance_RSSQ_subjMean)) %>% 
  print()
  

FigS4_new.A <- ggplot(facesData_summary, aes(x=Mean_Distance, y=Mean_Confidence)) +
  geom_point(alpha=0.5) +
  geom_smooth(method=lm, colour="black") +
  scale_x_continuous(name="Mean log distance (a.u.)") +
  scale_y_continuous(name="Mean confidence rating", limits=c(0, 1)) +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) +
  ggtitle("A.")

FigS4_new.B <- ggplot(facesData_summary, aes(x=Mean_RT, y=Mean_Confidence)) +
  geom_point(alpha=0.5) +
  geom_smooth(method=lm, colour="black") +
  scale_x_continuous(name="Mean RT take picture (ms)") +
  scale_y_continuous(name="Mean confidence rating", limits=c(0, 1)) +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) +
  ggtitle("B.")

FigS4_new.C <- ggplot(facesData_summary, aes(x=Mean_RT, y=Mean_Distance)) +
  geom_point(alpha=0.5) +
  geom_smooth(method=lm, colour="black") +
  scale_x_continuous(name="Mean RT take picture (ms)") +
  scale_y_continuous(name="Mean log distance (a.u.)") +
  theme(legend.position="none", 
        axis.title=element_text(size=10)
  ) +
  ggtitle("C.")


Figure_S4new <- FigS4_new.A + FigS4_new.B + FigS4_new.C
Figure_S4new

```


```{r correlateCovariates}

correlationBF(facesData_summary$Mean_Distance, facesData_summary$Mean_Confidence)
correlationBF(facesData_summary$Mean_RT,       facesData_summary$Mean_Confidence)
correlationBF(facesData_summary$Mean_RT,       facesData_summary$Mean_Distance)

cor.test(facesData_summary$Mean_Distance, facesData_summary$Mean_Confidence)
cor.test(facesData_summary$Mean_RT,       facesData_summary$Mean_Confidence)
cor.test(facesData_summary$Mean_RT,       facesData_summary$Mean_Distance)


```


```{r confidenceDistanceSlope_rebuttal3, warning=FALSE}

#load model
SubjConf_distanceMeasures.full <- brm(file = "brms_models/SubjConf_distanceMeasures.full")
summary(SubjConf_distanceMeasures.full)

ROPE <- rope_range(SubjConf_distanceMeasures.full)
ROPE

mcmc_slopeSamples_confidence <- SubjConf_distanceMeasures.full %>%
  spread_draws(b_log_distance_RSSQ) %>% 
  mutate(effect_estimate = b_log_distance_RSSQ) 

#count proportion of the posterior samples distribution that falls within the ROPE region
sum(mcmc_slopeSamples_confidence$effect_estimate > ROPE[1] & mcmc_slopeSamples_confidence$effect_estimate < ROPE[2], na.rm=TRUE)/length(mcmc_slopeSamples_confidence$effect_estimate)

```

```{r comparisonConfidenceSimilarity_rebuttal3, warning=FALSE}
#load model
similarity_distanceMeasures.full = brm(file = "brms_models/similarity_distanceMeasures.full")
summary(similarity_distanceMeasures.full)

mcmc_slopeSamples_similarity <- similarity_distanceMeasures.full %>%
  spread_draws(b_log_distance_RSSQ) %>% 
  mutate(effect_estimate = b_log_distance_RSSQ) 


#compute the relative slope estimate for confidence in relation to similarity - that is so that we can quantify how much this is, instead of putting a label (surprisingly limited etc) on it
confidenceVsDist_meanEstimate <-fixef(SubjConf_distanceMeasures.full)[2,1]
similarityVsDist_meanEstimate <-fixef(similarity_distanceMeasures.full)[2,1]
confidenceVsDist_meanEstimate/similarityVsDist_meanEstimate


#now get the confidence (credible?) interval for the ratio
# following this example 
# https://www.r-bloggers.com/2019/09/understanding-bootstrap-confidence-interval-output-from-the-r-boot-package/
posteriors_both <- data.frame("confidence" = mcmc_slopeSamples_confidence$effect_estimate, 
                              "similarity" =  mcmc_slopeSamples_similarity$effect_estimate)

get_ratio <- function(data, indeces) {
  # I need a single estimate, but boot takes many samples at once. There's no parameter So I just sample a single random one.
  d <- data[sample(indeces, size = 1), ] 
  ratio <- mean(d$confidence/d$similarity)
  ratio
}

boot_out <- boot(
  data = posteriors_both,
  statistic = get_ratio,
  R = 10000,
  stype = "i"
)

boot_out$t0
boot.ci(boot_out, type="norm")

```

```{r compute_individualModels_rebuttal3, warning=FALSE}

#Set priors and sampling parameters

heuristicPriors.full <- prior("normal(0,1.0/3)", class = "b", coef = "log_distance_RSSQ") + 
  prior(normal(0.8, 0.5), class = "Intercept")
heuristicPriors.null <- prior(normal(0.8, 0.5), class = "Intercept")

iterations =  15000 
warmup = 5000

for(subj in c(1:40)){
SubjConf_distanceMeasures_singleSubjs.full <- brm(confidence ~ log_distance_RSSQ + 
                                        (1+ log_distance_RSSQ |subject) + (1 |expression),
                                      data = facesData %>% filter(subject == subj), 
                                      family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.full,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = paste0("brms_models/SubjConf_distanceMeasures.full_singleSubj", subj))
}


```




```{r plot_individualModels_rebuttal3, warning=FALSE}

# create plotting function
makeSubjPlot <- function(subj,meanEstimate, l95percent, u95percent){
  
  plot <- SubjConf_distanceMeasures_singleSubjs.full %>%
    spread_draws(b_log_distance_RSSQ) %>% 
    mutate(effect_estimate = b_log_distance_RSSQ) %>%
    ggplot(aes(x = effect_estimate, fill = stat(ROPE[[1]] < x & x < ROPE[[2]]))) + 
    stat_halfeye(.width = c(.95)) +
    geom_vline(xintercept = c(-ROPE, ROPE), linetype = "dashed") +
    scale_fill_manual(values = c("gray80", "skyblue")) +
    theme(legend.position = "none",
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank(),
          axis.title.x=element_blank(),
          axis.text.y=element_blank(),
          axis.ticks.y=element_blank(),
          axis.title.y=element_blank(),
          plot.title = element_text(size = 6)) +
    ggtitle(paste0("M = ", meanEstimate, ", CI = [", l95percent, ",", u95percent, "]")) +
    labs(x = "Effect of ML-weighted distance", y = "Probability density")
}

plotList <- vector(mode = "list", length = 2)

for(subj in c(1:40)){
  
  # Load the estimated model
  SubjConf_distanceMeasures_singleSubjs.full <- brm(file = paste0("brms_models/SubjConf_distanceMeasures.full_singleSubj", subj))
  
  meanEstimate <-round(fixef(SubjConf_distanceMeasures_singleSubjs.full)[2,1], digits = 2)
  l95percent <-round(fixef(SubjConf_distanceMeasures_singleSubjs.full)[2,3], digits = 2)
  u95percent <-round(fixef(SubjConf_distanceMeasures_singleSubjs.full)[2,4], digits = 2)
  
  plotList[[subj]] <- makeSubjPlot(subj,meanEstimate, l95percent, u95percent)
  
}

#cowplot::plot_grid(plotlist = plotList, align = "hv") #this works, tooq
do.call("grid.arrange", c(plotList, ncol = 5))                               

```


