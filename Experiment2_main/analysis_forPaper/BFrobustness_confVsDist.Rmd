---
title: "Faces Analysis - Robustness check for BF10 for confidence vs distance"
author: "EF"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    toc: true
    toc_float:
      collapsed: false
      smooth_scroll: false
code_folding: show
number_sections: true
---

```{r setup, include=FALSE}
rm(list =ls())
library(tidyverse)
library(ggplot2)
library(here)
library(patchwork)
library(bayestestR)
library(brms)

```


### load data
```{r loadData}
facesData <- read.csv(here('ML_results_final.csv'))
facesData$subject <- as.factor(facesData$subject)

facesData.clean <- facesData %>% group_by(subject) %>% 
  filter(RT_takePic < quantile(RT_takePic, 0.95) ) %>% 
  filter(RT_takePic > 0.3) 

facesData <- facesData.clean 
# Add the log-transformed 
facesData$log_distance_RSSQ <- log(facesData$distance_RSSQ)

```

### copy the original priors
```{r setPriors}
heuristicPriors.full <- prior("normal(0,1.0/3)", class = "b", coef = "log_distance_RSSQ") + 
  prior(normal(0.8, 0.5), class = "Intercept")
heuristicPriors.null <- prior(normal(0.8, 0.5), class = "Intercept")

iterations =  15000 
warmup = 5000

```

### run original
```{r originalModel}
iterations_few =  1000 
warmup_few = 50
chains_few = 1

# Full model
SubjConf_distanceMeasures.full <- brm(confidence ~ log_distance_RSSQ + 
                                        (1+ log_distance_RSSQ |subject) + (1 |expression),
                                      data = facesData, family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.full,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = "brms_models/SubjConf_distanceMeasures.full")
summary(SubjConf_distanceMeasures.full)


# Null model
SubjConf_distanceMeasures.null <- brm(confidence ~ 1 + 
                                        (1+ log_distance_RSSQ |subject) + (1 |expression),
                                      data = facesData, family = gaussian(),
                                      cores = 4, chains = 4,
                                      prior = heuristicPriors.null,
                                      iter = iterations, warmup = warmup,
                                      save_all_pars = TRUE,
                                      sample_prior = TRUE,
                                      file = "brms_models/SubjConf_distanceMeasures.null")


BF_comparison <- bayestestR::bayesfactor_models(SubjConf_distanceMeasures.full, denominator = SubjConf_distanceMeasures.null)
BF_comparison


```


```{r robustnessCheck}
#Run robustness check


SubjConf_distanceMeasures.full.SD.prior0.1 <- update(SubjConf_distanceMeasures.full, 
                                                 prior = prior(normal(0,0.1), class = "b", coef = "log_distance_RSSQ") + 
                                                   prior(normal(0.8, 0.5), class = "Intercept"),
                                                 file=paste0("brms_models/SubjConf_distanceMeasures.full_SD.prior=0.1"))
summary(SubjConf_distanceMeasures.full.SD.prior0.1)
BF_comparison <- bayestestR::bayesfactor_models(SubjConf_distanceMeasures.full.SD.prior0.1, denominator = SubjConf_distanceMeasures.null)
BF_comparison


SubjConf_distanceMeasures.full.SD.prior0.7 <- update(SubjConf_distanceMeasures.full, 
                                                 prior = prior(normal(0,0.7), class = "b", coef = "log_distance_RSSQ") + 
                                                   prior(normal(0.8, 0.5), class = "Intercept"),
                                                 file=paste0("brms_models/SubjConf_distanceMeasures.full_SD.prior=0.7"))
summary(SubjConf_distanceMeasures.full.SD.prior0.7)
BF_comparison <- bayestestR::bayesfactor_models(SubjConf_distanceMeasures.full.SD.prior0.7, denominator = SubjConf_distanceMeasures.null)
BF_comparison


SubjConf_distanceMeasures.full.SD.prior1.1 <- update(SubjConf_distanceMeasures.full, 
                                                 prior = prior(normal(0,1.1), class = "b", coef = "log_distance_RSSQ") + 
                                                   prior(normal(0.8, 0.5), class = "Intercept"),
                                                 file=paste0("brms_models/SubjConf_distanceMeasures.full_SD.prior=1.1"))
summary(SubjConf_distanceMeasures.full.SD.prior1.1)
BF_comparison <- bayestestR::bayesfactor_models(SubjConf_distanceMeasures.full.SD.prior1.1, denominator = SubjConf_distanceMeasures.null)
BF_comparison

SubjConf_distanceMeasures.full.SD.prior1.4 <- update(SubjConf_distanceMeasures.full, 
                                                 prior = prior(normal(0,1.4), class = "b", coef = "log_distance_RSSQ") + 
                                                   prior(normal(0.8, 0.5), class = "Intercept"),
                                                 file=paste0("brms_models/SubjConf_distanceMeasures.full_SD.prior=1.4"))
summary(SubjConf_distanceMeasures.full.SD.prior1.4)
BF_comparison <- bayestestR::bayesfactor_models(SubjConf_distanceMeasures.full.SD.prior1.4, denominator = SubjConf_distanceMeasures.null)
BF_comparison





```

