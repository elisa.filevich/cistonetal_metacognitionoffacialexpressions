function [allSubjs] = step1_descriptionAndMetacog

dataDir = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment2/data/visual_dots';
addpath('/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment2/analysis_forPaper/visualdots/fx')
resultsPlots = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment2/analysis_forPaper/visualdots/diagnosticPlots';
resultsDir = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment2/analysis_forPaper/visualdots/';
if(~exist(resultsDir)); mkdir(resultsDir); end

subjectNames = regexpdir(dataDir,'^[0-9]', 0); %get non-recursively

subplotSet.rows = 4;
subplotSet.cols = 3;

for subj = 1:length(subjectNames)
    
    if subj ~= 8 %subject 8 (110JA) did not complete the visual dots task
        
        %prepare figure for all analyses
        figHandle = figure('Name', subjectNames{subj}); hold on
        
        
        %% First check: Make sure that training had mostly correct trials
        
        %% Look at the initial staricase
        if subj ~= 22
            cd([subjectNames{subj} '/subj' subjectNames{subj}(end-5:end-1) '_audio0_visual1_staircase1_train0/'])
            fullDataFile = dir('alldat*.mat');
            load(fullDataFile.name);
            
            stair = zeros(1,length(Info.T));
            strongDirection = zeros(1,length(Info.T));
            for tr = 1:length(Info.T)
                stair(tr) = Info.T(tr).stair;
                vision.signalDiffs(tr)= Info.T(tr).vision.signalDiff;
                strongDirection(tr) = Info.T(tr).vision.strongDirection;
            end
        else
            stair = NaN;
            vision.signalDiffs = NaN;
            strongDirection = NaN;
        end
        
        %plot the initial staircase
        subplotSet.position = 1;
        getStaircase(figHandle, subplotSet, vision, stair)
        
        
        %% Then analyze the data properly
        cd([subjectNames{subj} '/subj' subjectNames{subj}(end-5:end-1) '_audio0_visual1_staircase0_train0/'])
        fullDataFile = dir('alldat*.mat');
        load(fullDataFile.name);        
        
        stair = zeros(1,length(Info.T));
        strongDirection = zeros(1,length(Info.T));
        conf = zeros(1,length(Info.T));
        for tr = 1:length(Info.T)
            stair(tr) = Info.T(tr).stair;
            vision.signalDiffs(tr)= Info.T(tr).vision.signalDiff;
            strongDirection(tr) = Info.T(tr).vision.strongDirection;
            conf(tr) = Info.T(tr).VAS.confidence;
            RT2(tr) = Info.T(tr).VAS.movRT;
        end
        RT      = [Info.T.RT];
        correct = [Info.T.ResponseCorrect];
        %Correct a bug that was in the VAS code
        conf = conf-min(conf);     
        %Reconstruct the response based on stim and accuracy
        response(find(correct)) = strongDirection(find(correct));
        response(find(~correct)) = 3 - strongDirection(find(~correct));
        
        %% Clean trials with too low or high RT for either response
        allSubjs.shortRT1count(subj) = sum(RT < .2);
        allSubjs.longRT1count(subj) = sum(RT > 3);
        allSubjs.shortRT2count(subj) = sum(RT2 < .2);
        allSubjs.longRT2count(subj) = sum(RT2 > 5);
        
        trialsToRemove = find(RT < .3 | RT > 3 | RT2 < .3 | RT2 > 5);
        correct(trialsToRemove) = [];
        conf(trialsToRemove) = [];
        strongDirection(trialsToRemove) = [];
        RT(trialsToRemove) = [];
        RT2(trialsToRemove) = [];
        response(trialsToRemove) = [];
        
        %% calculate and plot stuff
        subplotSet.position = subplotSet.position + 1;
        getStaircase(figHandle, subplotSet, vision, stair)
        
        subplotSet.position = subplotSet.position + 1;
        allSubjs.percentCorrect(subj,:) = getPercentCorrect(figHandle, subplotSet, correct);
        
        subplotSet.position = subplotSet.position + 1;
        getReactionTimes(figHandle, subplotSet, RT, correct)
        
        subplotSet.position = subplotSet.position + 1;
        confVsRT(figHandle, subplotSet, RT, conf)
        
        subplotSet.position = [subplotSet.position + 1, subplotSet.position + 2];
        allSubjs.SSEfit(subj) = metadPrime(figHandle, subplotSet, conf, strongDirection, response);
        
        subplotSet.position = subplotSet.position(2);
        subplotSet.position = [subplotSet.position + 1, subplotSet.position + 2];
        checkConfBiases(figHandle, subplotSet, strongDirection, correct, conf)
        
        subplotSet.position = subplotSet.position(2) + 1;
        allSubjs.AUROC2(subj) = Aroc(figHandle, subplotSet, conf, correct);

        subplotSet.position = [subplotSet.position + 1, subplotSet.position + 2 ]; 
        confvscorrectIncorrect(figHandle, subplotSet, conf, correct);
        
        %get the time the experiment took for each subject
        allSubjs.durationInMinutes(subj) = experimentDuration/60;
        
        save([resultsDir '/metavisual_all.mat'])     
        %savefig(figHandle, [resultsPlots '/metavisual_subj' num2str(subj) '.fig'])
    end
    
end
end


%%

function getStaircase(figHandle, subplotSet, vision, stair)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
title('staircase')
plot(vision.signalDiffs(stair==1), 'bo-');
plot(vision.signalDiffs(stair==2), 'ro-');
xlabel('trial number'); ylabel('signalDiff (sec)');

%TODO Maybe also -as a sanity check- plot the relative number of dots to make
%sure this is correct?

end

%%
function percentCorrect = getPercentCorrect(figHandle, subplotSet, correct)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
title('type1 task')
b = bar([sum(correct) / length(correct), ...
    sum(~correct / length(correct))]);
set(gca, 'XTickLabel', {'correct', 'incorrect'}, 'XTick', 1:2)
ylabel('% trials')
percentCorrect = sum(correct) / length(correct)*100;

end

%%
function getReactionTimes(figHandle, subplotSet, RT, correct)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
nbins = 10;
histogram(RT(correct>0), min(RT):range(RT)/nbins:max(RT), 'FaceColor', [0 .5 0]);
histogram(RT(correct==0), min(RT):range(RT)/nbins:max(RT), 'FaceColor', [.5 0 0]);
legend('correct', 'incorrect')
xlabel('type1 RT (sec)')
ylabel('count')

end


function confVsRT(figHandle, subplotSet, RT, conf)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
title('Confidence vs type1RT')
scatter(RT, conf)
lsline
xlabel('RT (secs)')
ylabel('confidence')

end


function SSEfit = metadPrime(figHandle, subplotSet, conf, strongDirection, response)

figure(figHandle)

Nratings = 4; %define how many confidence levels we want
conf_binned = discretize(conf, Nratings);
%conf_binned = discretize(conf, min(conf):range(conf)/Nratings:max(conf)); % for Matlab 2016a the line above does not work

%Acount for bins with 0 trial counts in nR_SX vectors
cellpad = 1;
%Usign strongDirection-1 and response-1 because trial2counts expects 0/1s
%and not 1/2s as we have here
[nR_S1 nR_S2] = trials2counts(strongDirection-1,response-1,conf_binned,Nratings,cellpad);

SSEfit = type2_SDT_SSE(nR_S1, nR_S2);

%Plot if it worked
figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1)); hold on
title('Meta-d analysis')
b = bar([SSEfit.meta_da, SSEfit.da, SSEfit.M_ratio, SSEfit.M_diff]);
set(gca, 'XTickLabel', {'Meta-d', 'd', 'Mratio', 'Mdiff'}, 'XTick', 1:4)

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(2))
plot(nR_S1,'ro-'); hold on
plot(nR_S2,'b+-');
legend('nRS1', 'nRS2')

end


function auroc2  = Aroc(figHandle, subplotSet, conf, correct)

figure(figHandle)

Nratings = 4; %define how many confidence levels we want
conf_binned = discretize(conf, Nratings);
%conf_binned = discretize(conf, min(conf):range(conf)/Nratings:max(conf)); % for Matlab 2016a the line above does not work

[auroc2, cum_FA2, cum_H2] = type2roc(correct, conf_binned, Nratings);

figure(figHandle)
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
color = 'b';
plot(cum_FA2,cum_H2, [color, '-o']);
axis square
set(gca,'YLim',[0 1]);
set(gca,'XLim',[0 1]);
title ('type 2 ROC')
xlabel('FA2'); ylabel('HITS2')

end

function checkConfBiases(figHandle, subplotSet, strongDirection, correct, conf)
%% Check if the type 1 choice (left or right) biases confidence judgements (lower or higher)

conf1 = conf((strongDirection==1&correct==1)|(strongDirection==2&correct==0));
conf2 = conf((strongDirection==1&correct==0)|(strongDirection==2&correct==1));

figure(figHandle)
nbins = 10;
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1))
histogram(conf1, min(conf):range(conf)/nbins:max(conf), 'FaceColor', [0 .5 0]);
title('left')
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(2))
histogram(conf2, min(conf):range(conf)/nbins:max(conf), 'FaceColor', [.5 0 0]);
title('right')
xlabel('confidence')
ylabel('count')

disp('mean1')
disp(mean(conf1(~isnan(conf1))))
disp('mean2')
disp(mean(conf2(~isnan(conf2))))

end


%added confidence distributions vs. correct/incorrect

function confvscorrectIncorrect(figHandle, subplotSet, conf, correct)
%% Check if the type 1 performance (correct or incorrect) biases confidence judgements (lower or higher)

conf1 = conf(correct==1);
%conf2 = conf((strongDirection==1&correct==0)|(strongDirection==2&correct==1));
conf2 = conf(correct==0);

%    figure('Name', subjectNames{subj}); hold on
figure(figHandle)
nbins = 10;
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(1))
histogram(conf1, min(conf):range(conf)/nbins:max(conf), 'FaceColor', [0 .5 0]);
title('correct')
subplot(subplotSet.rows,subplotSet.cols,subplotSet.position(2))
histogram(conf2, min(conf):range(conf)/nbins:max(conf), 'FaceColor', [.5 0 0]);
title('incorrect')
xlabel('confidence')
ylabel('count')


end

%%
%

% conf = getfield(ans, 'SSEfit');
%
% % get Mratios only
%
% Mratio = [conf.M_ratio]';
%
% % %save Mratios
%
% %struct2csv(Mratio, [resultDir 'Mratio.csv']);
%
% csvwrite([resultDir 'Mratio.csv'], Mratio);
%
% save([resultDir 'Mratio.mat'], 'Mratio');
%
% disp(['saved Mratio files to' resultDir])
