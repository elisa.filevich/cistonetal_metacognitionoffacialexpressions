function setDotParams()

global dev
global cfg

%% figure out screen params for circles

screenRect = dev.screenRect;

%horizontalResolution    = screenRect(3)-screenRect(1);                     % screen resolution in pixels
imVisang                = 5;                                                % visual angle of stimulus in degrees
ScreenWidthDegrees      = visang(cfg.viewDist, [], dev.widthOfScreen_inCm);
pixPerDeg               = (screenRect(3) - screenRect(1))/ScreenWidthDegrees;

imsize      = imVisang * pixPerDeg;                                         %image size

%% Stimulus Locations

dfc = 5.5;                                                                  %eccentricity in degrees
eccen = ceil(screenRect(3) * cfg.viewDist * tan(pi*dfc/180)/dev.widthOfScreen_inCm(1));

if cfg.modality.vision == 1
    cfg.stim.vision.gratingRect(:,1) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect), -eccen, 0); %left  centre
    cfg.stim.vision.gratingRect(:,2) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect),  eccen, 0); %right centre
else
    cfg.stim.vision.gratingRect(:,1) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect), -eccen, -eccen/2); %left  bottom
    cfg.stim.vision.gratingRect(:,2) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect),  eccen, -eccen/2); %right bottom
    cfg.stim.vision.gratingRect(:,3) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect), -eccen,  eccen/2); %left  top
    cfg.stim.vision.gratingRect(:,4) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect),  eccen,  eccen/2); %right top
end

cfg.stim.vision.centers = [cfg.stim.vision.gratingRect(3,:)+cfg.stim.vision.gratingRect(1,:); cfg.stim.vision.gratingRect(4,:)+cfg.stim.vision.gratingRect(2,:)]/2;

%% set parameters

[winsize(1), winsize(2)] = Screen('WindowSize', dev.wnd);
dev.fov = min(winsize);

cfg.stim.vision.dotcolor = [0 0 0];
cfg.stim.vision.dotsize = 0.03;
cfg.stim.vision.REF = 50;

% Diameter of the circle relative to the screen
cfg.stim.vision.inner_circle = imsize / dev.fov;
cfg.stim.vision.pen_width = cfg.stim.vision.inner_circle / 20;


end