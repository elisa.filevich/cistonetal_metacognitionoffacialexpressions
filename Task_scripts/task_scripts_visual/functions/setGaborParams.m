function setGaborParams()

global dev
global cfg

%% and figure out visual angle to pixel parameters

screenRect = dev.screenRect;


%get resolution in pixels. Use this 'manual' way as opposed to Screen(Resolution')
%widthOfScreen_inPixels  = screenRect(3) - screenRect(1);
%heightOfScreen_inPixels = screenRect(4) - screenRect(2);

%calculate pix per Cm for this display
%pixPerCm(1)             = widthOfScreen_inPixels/Info.visual.widthOfScreen_inCm;
%pixPerCm(2)             = heightOfScreen_inPixels/Info.visual.heightOfScreen_inCm;
%pixPerCm                = mean(pixPerCm);

% now, every time you want to calculate the size in pixels of a visual
% stimulus given a visual angle , you can do:
% stimPix = tan(visAng/2) * 2 * viewDist * pixPerCm;
% ACHTUNG! visAng must be in rad.

% get the x and y centres
%XcentreScreen            = (screenRect(3)-screenRect(1))/2;
%YcentreScreen            = (screenRect(4)-screenRect(2))/2;

%% figure out screen params for gabors
%horizontalResolution    = screenRect(3)-screenRect(1);                      % screen resolution in pixels
imVisang                = 5;                                                % visual angle of stimulus in degrees
imSigmaDeg              = imVisang * .2;                                    % fit gaussian window into image frame
ScreenWidthDegrees      = visang(cfg.viewDist, [], dev.widthOfScreen_inCm);
pixPerDeg               = (screenRect(3) - screenRect(1))/ScreenWidthDegrees;

%nLocs       = 2;                                                            %number of gabor locations

%stepSize    = .03;

imsize      = imVisang * pixPerDeg;                                         %image size
% lamda       = 6.896;                                                      %wavelength in pixels  %% makes 3 cpd
lamda       = 5;                                                            %wavelength in pixels
sigma       = imSigmaDeg * pixPerDeg ;                                      %gaussian standard deviation in pixels
phase       = 0;                                                            %phase 0:1
Ltheta      = 0;                                                            %local orientation in degrees (clockwise from vertical)
Gtheta      = 0;                                                            %global orientation in degrees (clockwise from vertical)
fdist       = 0;                                                            %distance between target and flankes in pixels
xoff        = 0;                                                            %horizontal offset position of gabor in pixels
yoff        = 0;                                                            %vertical offset position of gabor in pixels
cutoff      = -.05;                                                         %if >0, applies threshold of gauss>cutoff to produce sharp edges and no smooth fading
%if <0, trims off gauss > abs(cutoff) while preserving fading in remaining regions
show        = 0;                                                            %if present, display result


%% make gabors

%%make base gabor only
base_Grate      = gabor_pix(imsize, lamda, sigma, phase, Ltheta, Gtheta, fdist, xoff, yoff, cutoff, show);   %make gabor
base_im_dim     = base_Grate * cfg.stim.vision.baseContrast;                                  % modulate contrast
base_imP        = (base_im_dim'+1)/2;                                       % convert -1->1 to 0->1
base_imG        = round(255 .* (base_imP .^  (1 / dev.gamma)));             % apply gamma correction to image
cfg.stim.vision.base_GaborTex   = Screen('MakeTexture', dev.wnd, base_imG);
cfg.stim.vision.base_Grate      = base_Grate;

%% THIS COMMENTED BIT OF CODE WILL PREDEFINE THE GABORS TO USE WITH FIXED CONTRASTS
%
% grates=cell(1,length(contrast));
% for gab=1%:length(contrast)
%     grates{gab}=gabor_pix(imsize, lamda, sigma, phase, Ltheta, Gtheta, fdist, xoff, yoff, cutoff, show);   %make gabor
%     im_dim{gab} =  grates{gab}* contrast(gab);                            % modulate contrast
%     imP{gab} = (im_dim{gab}'+1)/2;                                        % convert -1->1 to 0->1
%     imG{gab}= round(255 .* (imP{gab} .^  (1 / gam)));                     % apply gamma correction to image
%     %imageRGB{gab} = repmat(imG{gab}(:),1,3);                             % replicate vectorized image matrix three times for RGB
%
% end %% loop making gabors

%% make gabor patches with different contrasts

%actually do not make in advance because the stupid quest will do whatever
%it wants. (or well, the quest suggestion could be taken as the find
%nearest. for now try how it goes with many)
% for c = 1:length(contrast)
% GaborTex(c) = Screen('MakeTexture', win, imG{c});
% end

%baseGaborTex = GaborTex(1);



%% gabor Locations

dfc = 5.5;                                                                  %eccentricity in degrees
eccen = ceil(screenRect(3) * cfg.viewDist * tan(pi*dfc/180)/dev.widthOfScreen_inCm(1));

if cfg.modality.vision == 1
    cfg.stim.vision.gratingRect(:,1) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect), -eccen, 0); %left  centre
    cfg.stim.vision.gratingRect(:,2) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect),  eccen, 0); %right centre
else
    cfg.stim.vision.gratingRect(:,1) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect), -eccen, -eccen/2); %left  bottom
    cfg.stim.vision.gratingRect(:,2) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect),  eccen, -eccen/2); %right bottom
    cfg.stim.vision.gratingRect(:,3) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect), -eccen,  eccen/2); %left  top
    cfg.stim.vision.gratingRect(:,4) = OffsetRect(CenterRect(SetRect(0,0,imsize,imsize),screenRect),  eccen,  eccen/2); %right top
end





