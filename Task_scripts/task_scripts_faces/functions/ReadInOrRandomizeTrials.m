function data = ReadInOrRandomizeTrials

global cfg;

%Two alternatives here:
%1. Use the .xls-based config files
%[data,cfg] = expTools.configFromXLS( cfg.configFile , cfg );

%2. better: just use the lines below:
%rng(12345);        % For reproducible randomization
rng('shuffle')

data = cell(cfg.blocks,1);

for block = 1 : cfg.blocks
    data{block} = cell(length(cfg.expressions),1);
end

%Set block 1 first, where we choose actor-target images
for block = 1
    shuffledActorsExpressions = Shuffle(1: numel(cfg.expressions));
    for trial = 1 : length(shuffledActorsExpressions)
        data{block}{trial} = struct( 'facialexpression', shuffledActorsExpressions(trial),...
            'getpicturefromblock', NaN);
    end
end

%Set remaining blocks, where we show previously-recorded subject-target images
for block = 2 : cfg.blocks
    blocktogetpictures = 1;     % Always get picture from block 1
    getpicturefromtrial = [];
    for rep = 1 : cfg.repetitionsPerBlock
        getpicturefromtrial = [getpicturefromtrial Shuffle(1: numel(cfg.expressions))];
    end
    
    for trial = 1 : length(getpicturefromtrial)
        data{block}{trial} = struct( ...
            'facialexpression', data{blocktogetpictures}{getpicturefromtrial(trial)}.facialexpression,...
            'getpicturefromblock', blocktogetpictures, ...       
            'getpicturefromtrial', getpicturefromtrial(trial));
    end
    
    
    
end

