function p=getfullpath(p)

    oldp = pwd();
    
    if(exist( p, 'dir') )
        % not the fastest solution but matlabs simplest as it seems
        cd(p);
        p = pwd();
        cd(oldp);
    else
        error(['path ''' p ''' not found.']);
    end
end
