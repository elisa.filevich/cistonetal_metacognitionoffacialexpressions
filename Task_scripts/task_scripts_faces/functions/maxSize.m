function s = maxSize( rect, c )

    s = fliplr(max(rect(3:4)) ./ rect(3:4));
    
    if nargin > 1
        if (ischar(c) && c == 'x') || (isnumeric(c) && c == 1 )
            s = s(1);
        elseif (ischar(c) && c == 'y') || (isnumeric(c) && c == 2 )
            s = s(2);
        end
    end    
end