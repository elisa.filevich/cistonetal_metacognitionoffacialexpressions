function arr= saveToXLS( file, data, cfg )
%[ data, cfg ] = configFromXLS( [file] [, cfg] ) configures experiment with structured xls-file

    arr = cell(0,100);
    
    fields = fieldnames(cfg);
    for k =1:numel(fields)
        
        if strcmp( fields(k), 'cHeader')
            continue;
        end
        
        cfgvar = cfg.(fields(k));        
        
        if ischar( cfgvar ) 
            arr= addrow( arr, [fields(k), {cfgvar}] );
        elseif iscell( cfgvar )
            arr= addrow( arr, [fields(k), cfgvar] );
        else
            arr= addrow( arr, [fields(k), num2cell(cfgvar)] );
        end
    end
    
    arr= addrow( arr, {cfg.sExperimentDelimiter} );
    arr= addrow( arr, cfg.cHeader );
    
    for l=1:numel(data)
        arr=addrow( arr, {cfg.sBlockDelimiter} );
        
        for m=1:numel(data{l})
            trialrow = cell(1, numel( cfg.cHeader ));
            
            % fill in the data
            for k=1:numel( cfg.cHeader )
                if isfield( data{l}{m}, cfg.cHeader(k) )
                    trialrow(k) = data{l}{m}.(cfg.cHeader(k));
                end
            end
            
            % print some information about experiment<->configuration misadjustments
            ignoredcols = setdiff( cfg.cHeader, fieldnames( data{l}{m} ) );
            if ~isempty( ignoredcols )
                warning( '%d/%d: column(s) not specified by experiment: %s',...
                     l,m, strjoin(', ', ignoredcols) );
            end
            
            ignoredvars = setdiff( fieldnames( data{l}{m} ), cfg.cHeader );
            if ~isempty( ignoredvars )
                warning( '%d/%d: variable(s) in data-array not mentioned in header: %s',...
                     l,m, strjoin(', ', ignoredvars) );
            end
            
            arr=addrow( arr, trialrow );
        end
    end
    
    function arr=addrow( arr, row )
        
        d = cell(1,100);
        d(1:numel(row)) = row;
        arr = [arr;d];
    end

    
    xlswrite( file, arr );
end


