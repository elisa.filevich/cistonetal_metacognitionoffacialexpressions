function [ stri ] = strrep_struct( stri, stru )
%newstring = strrep_struct( oldstring, struct ) replace tokens in a string by a struct
%   example:
%       strrep_struct( '%id%-%trial%', struct('id',1,'trial',12) )
%
%       would return '1-12'
%

    f = fieldnames(stru);
    for i=1:numel(f)
        stri = strrep( stri, ['%' f{i} '%'], num2str( stru.( f{i} ) ) );
    end
end

