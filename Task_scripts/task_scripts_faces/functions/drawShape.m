function drawShape( shape, wnd )
    % [] = drawShape ( shape , wnd ) draw a shape into PsychToolBox Window
    %
    %   'shape' is a struct defining a to-be-drawn shape as follows:
    %
    %   general fields:
    %       type        string      {arc|circle|rect|poly|texture}
    %       hide        bool
    %       pos         vector      [x, y]
    %       size        vector      [w, h]
    %       ori         double      Orientation in degrees
    %       fillColor   vector      PsychToolBox Color
    %       borderColor vector         -"-
    %   type circle:
    %       radius      float
    %   type arc:
    %       radius      float or
    %       size        vector      [x, y]; radius overwrites size
    %       startAngle  double      degrees. 0 corresponds to horizontal
    %                               axis upwards from position
    %       arcAngle    double      degrees. Size of arc
    %   type rect:
    %       size        vector      [x, y]
    %
    %   type poly:
    %       points      matrix      Nx2 for N points (x,y) of a polygon
    %
    %   type texture:
    %       size        vector      [x, y]
    %       texture     handle      handle to PsychToolBox-Texture
    %       
    %   the positions are relative to the center (0,0)
    %   the sizes are normed to the smallest dimension:
    %   for a resolution of 1024x768 (=> 1.33/1)
    %               x (horizontal)    goes from -0.66 to 0.66 (=> 1.33)
    %               y (vertical)    goes from -0.5 to 0.5 (=> 1)
    %
    %   example (draws a circle with radius half of the smallest dimension):
    %   >> circle = struct('type', 'circle', 'radius', 0.5);
    %   >> expTools.drawShape( circle );
    
    
    if isfield( shape, 'hide' ) && shape.hide
        return
    end

    if ~isfield( shape, 'type')
        %warning('drawShape:type', 'no shape type specified, guessing one' );
        if isfield( shape, 'startAngle') && isfield( shape, 'arcAngle')
            shape.type = 'arc';
        elseif isfield( shape, 'texture' )
            shape.type = 'texture';
        elseif isfield( shape, 'radius')
            shape.type = 'circle';
        elseif isfield( shape, 'points')
            shape.type = 'poly';
        elseif isfield( shape, 'size')
            shape.type = 'rect';
        end

    end
    
    %some default values for position and orientation
    if ~isfield( shape, 'pos' ) || size(shape.pos, 2) ~= 2
        shape.pos = [0 0];
    end
    
    if ~isfield( shape, 'ori' )
        shape.ori = 0;
    end
    
    if ~isfield( shape, 'size')
        shape.size = [0.1 0.1];
    end
    
    if ~strcmp(shape.type, 'texture') && ...
            ~( isfield( shape, 'fillColor') || isfield( shape, 'borderColor' ) )
        shape.fillColor = [255,255,0];
    end
    
    wndRect = Screen('Rect', wnd);

    aspectRatio = fliplr( wndRect(3:4) / max(wndRect(3:4)));
    centerPos = [0.5 0.5] ./ aspectRatio;
    
    if strcmp( shape.type, 'circle')
        %convert circle into arc
        shape.type='arc';
        shape.startAngle = 0;
        shape.arcAngle = 360;
    end
    
    if strcmp( shape.type, 'texture' ) && ~isfield( shape, 'texture' )
        warning('drawShape:texture', 'no texture specified, drawing white rect');
        shape.fillColor = [255, 255, 255];
        shape.type = 'rect';
    end
    
    if (strcmp(shape.type, 'texture') || strcmp( shape.type, 'rect' )) 
        
        if mod(shape.ori, 180) == 0 || strcmp(shape.type, 'texture' )
            %calculate rect if we want to draw a rect with orientation 0
            rect = [ aspectRatio .* wndRect(3:4) .* (centerPos + shape.pos - shape.size/2), ...
                            aspectRatio .* wndRect(3:4) .* (centerPos + shape.pos + shape.size/2) ];
        else
            %for a rotated rect, we need to fall back to a poly
            shape.type = 'poly';
            shape.points = [shape.size;...
                           [-shape.size(1) shape.size(2)];...
                            -shape.size;...
                            [shape.size(1) -shape.size(2)] ]/2;
            %rotation is applied later
        end
    end
    
    if strcmp( shape.type, 'arc' )
        % get size or radius
        if isfield( shape, 'radius' )
            shape.size = shape.radius;
        elseif ~isfield( shape, 'size' )
            warning('drawShape:arc', 'no radius nor size specified');
            return;
        end

        if ~isfield( shape, 'startAngle') || ~isfield( shape, 'arcAngle')
            warning('drawShape:arc', 'angles of arc not specified');
            return;
        end
        % calculate position
        rect = [ aspectRatio .* wndRect(3:4) .* (centerPos + shape.pos - shape.size/2), ...
                       aspectRatio .* wndRect(3:4) .* (centerPos + shape.pos + shape.size/2) ];
            
        if isfield( shape, 'ori' ) && shape.ori ~= 0
            if numel(shape.size) > 1 && shape.size(1) ~= shape.size(2)
                warning( 'drawShape:arc', 'drawing elliptic, oriented arc not yet supported' );
            else
                shape.startAngle = shape.startAngle + shape.ori;
            end
        end            
            
    end
    
    if strcmp( shape.type, 'poly' )
        if  mod(shape.ori, 360) ~= 0
            %rotate polygon according to orientation
            shape.points = shape.points * [ cos(shape.ori/180*pi), sin(shape.ori/180*pi); ... 
                                           -sin(shape.ori/180*pi), cos(shape.ori/180*pi)];
        end
        
        shape.points =  repmat( aspectRatio .* wndRect(3:4), size(shape.points, 1), 1) .* (...
                           repmat( centerPos + shape.pos, size(shape.points, 1), 1) + shape.points );
    end    
    
    if strcmp( shape.type, 'texture' ) 
        Screen('DrawTexture', wnd, shape.texture, [], rect, shape.ori );
        return
    end
    if isfield( shape, 'fillColor' )
        shape.fillColor = checkColor( shape.fillColor );
        switch shape.type
            case 'arc'
                Screen('FillArc', wnd, shape.fillColor, rect, shape.startAngle, shape.arcAngle);
            case 'rect'
                disp(rect);
                Screen('FillRect', wnd, shape.fillColor, rect ); 
            case 'poly'
                Screen('FillPoly', wnd, shape.fillColor, shape.points );
        end
    end
    if isfield( shape, 'borderColor' )
        shape.borderColor = checkColor( shape.borderColor );
        switch shape.type
            case 'arc'
                Screen('FrameArc', wnd, shape.borderColor, rect, shape.startAngle, shape.arcAngle);
            case 'rect'
                Screen('FrameRect', wnd, shape.borderColor, rect ); 
            case 'poly'
                Screen('FramePoly', wnd, shape.borderColor, shape.points );
        end
        
    end
    
    % fix colors (PTB needs integers 0..255)
    % TODO : [1 1 1] almost black or completely white?
    function colors = checkColor( colors )
        if any( colors > 1 )
            colors = round(colors);
        elseif any( and( colors <= 1, colors >= 0) )
            colors = round( colors * 255 );
        end
    end
end

