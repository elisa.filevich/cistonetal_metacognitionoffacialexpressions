%read in all files in a given folder
clear all;
close all

%% READ IN XLS TRIAL INFORMATION.
%% MAKE SURE XLS FILES ARE EXCEL 95

rootDir = '/Users/elisa/Documents/1_academic_backup/postdoc_MPI/facesMetacog_task/preprocessedData'; 

subjects = {'101_PicWord', '102_PicWord', '103_PicWord', '104_PicWord', '106_PicWord', '107_PicWord', '201_PicWord', '202_PicWord', '203_PicWord', '204_PicWord', '205_PicWord', '206_PicWord', '207_PicWord'};

s = 1;

load([rootDir 'preprocessed_subj_00160' subjects{s} '.mat'])



%% HAVE A LOOK AT THE PROCRUSTES

%% 
%subject 1, expression 25 is interesting
expr = 20;                                                    %choose any number between 1 and 30

figure; hold on; title(['Procrustes aligned face ' num2str(expr)])

oneLineAtATime = 0; %1 or 0

for r = 1:8
    
    subplot(1,2,1);
    plot(targetFace{expr}(:,1), -targetFace{expr}(:,2),'r', 'LineWidth', 2)
    hold on
    plot(originalFace{expr,r}(:,1), -originalFace{expr,r}(:,2) )
    title('original faces')
    axis equal;
    if oneLineAtATime; hold off; end
    
    subplot(1,2,2);
    %plot(alignedFace{expr,r}(:,1), -alignedFace{expr,r}(:,2) )
    plot(preprocessed.procrustesAligned{expr,r+1}(:,1), -preprocessed.procrustesAligned{expr,r+1}(:,2) )
    
    hold on
    plot(preprocessed.procrustesAligned{expr,1}(:,1), -preprocessed.procrustesAligned{expr,1}(:,2),'r', 'LineWidth', 2)
    axis equal
    frameNumber = picturePairs(expr,r);
    title('procrustes-aligned')
    if oneLineAtATime;
        title(['response num ' num2str(frameNumber)]); hold off;  pause
    end
end


