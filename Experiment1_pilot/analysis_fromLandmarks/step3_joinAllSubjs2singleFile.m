
%clear all
clear all
close all

global subjects_of_interest
global inputDir inputDirExternalRatings
global allSubjs_aggregated

cd('/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment1/analysis_fromLandmarks')
addpath('functions')

%% Decide what you want to measure
subjs_numIndex = 1:13;     
subjectsName = 'all';   
savedata = 1;        %only save to csv if you're running all subjs
myExpressions = 1:30;     
distanceName = 'rssq';
selectedPointsName = 'allLandmarks';

%% READ IN XLS TRIAL INFORMATION.
%% MAKE SURE XLS FILES ARE EXCEL 95

inputDir                = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment1/data/preprocessedData';
inputDirExternalRatings = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment1/data/externalRatings';
outputDir               = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment1/data/preprocessedData';

subjects_of_interest = {'101_PicWord', '102_PicWord', '103_PicWord', '104_PicWord', '106_PicWord', '107_PicWord', ...
    '201_PicWord', '202_PicWord', '203_PicWord', '204_PicWord', '205_PicWord', '206_PicWord', '207_PicWord'}; %allsubjects at this point


%%  calculate the distances for each picture and join with behavioural data and ratings
getResponses(subjs_numIndex, myExpressions);

allSubjs_aggregated.confidenceTransformed16 = 7 - allSubjs_aggregated.confidence;
allSubjs_aggregated.similarityJudge2Transformed16 = 7 - allSubjs_aggregated.rater2;
allSubjs_aggregated.similarityJudge3Transformed16 = 7 - allSubjs_aggregated.rater3;
allSubjs_aggregated.similarityJudge4Transformed16 = 7 - allSubjs_aggregated.rater4;
allSubjs_aggregated.similarityJudge5Transformed16 = 7 - allSubjs_aggregated.rater5;

allSubjs_aggregated

%% export to csv, analysis continues in R
%then simply write it
if savedata
    save([outputDir filesep 'facesMetacog_allData.mat'], 'allSubjs_aggregated');
end




