%read in all files in a given folder
clear all;
close all

%% READ IN XLS TRIAL INFORMATION.
%% MAKE SURE XLS FILES ARE EXCEL 95

rawBehaveDataDir = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment1/data/'; %inputdir
preprocessedDir = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment1/data/preprocessedData'; %outputdir
addpath('/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment1/analysis_fromLandmarks/functions')
subjects = {'101_PicWord', '102_PicWord', '103_PicWord', '104_PicWord', '106_PicWord', '107_PicWord', '201_PicWord', '202_PicWord', '203_PicWord', '204_PicWord', '205_PicWord', '206_PicWord', '207_PicWord'};

%%  set up identity of points
%these numbers correspond to the indexes of the 99 points in the array
faceOutline     = 1:22;         colorCode(faceOutline)      = 1;
noseVertical    = 23:26;        colorCode(noseVertical)     = 2;
noseHorizontal  = 27:29;        colorCode(noseHorizontal)   = 3;
R_eyeOutline    = 30:37;        colorCode(R_eyeOutline)     = 4;
L_eyeOutline    = 38:45;        colorCode(L_eyeOutline)     = 5;
mouth_out       = 46:66;        colorCode(mouth_out)        = 6;
mouth_in        = 67:85;        colorCode(mouth_in)         = 7;
eyebrow_R       = 86:91;        colorCode(eyebrow_R)        = 8;
eyebrow_L       = 92:97;        colorCode(eyebrow_L)        = 9;
R_pupil         = 98;           colorCode(R_pupil)          = 10;
L_pupil         = 99;           colorCode(L_pupil)          = 11;
faceOut_noJaw   = [1:4 11:22];  %colorCode(faceOut_noJaw)    = 12;  %this will mess up the plots
invariant5points = [30 34 38 42 28];

% To see them use:
% plot(points(:,1,1),-points(:,2,1), 'b');hold on
% plot(points(30,1,1),-points(30,2,1), 'rO') %30: inner right eye (seen as a mirror)
% plot(points(34,1,1),-points(34,2,1), 'rO') %34: outer right eye
% plot(points(38,1,1),-points(38,2,1), 'rO') %38: inner left eye
% plot(points(42,1,1),-points(42,2,1), 'rO') %42: outer left eye
% plot(points(28,1,1),-points(28,2,1), 'rO') %28: point below nose

%% set the xls column corresponding to the getPictureFromTrial info
fromTrialCol     = 4;
trialNumCol      = 1;
nBlocks          = 2;
ratingCol        = 10;
RT_imageCol      = 7;
RT_ratingCol     = 9;


for s = 1:length(subjects)
        
    %% GET THE FILE NAMES AND CORRESPONDENCE
    subj = subjects{s};
    
    %open xls file
    xlsDir  = [rawBehaveDataDir filesep 'data_rawData' filesep subj];
    xlsFile = dir(fullfile(xlsDir, [subj(1:3) '*.xls']));
    
    [num txt raw] = xlsread([xlsDir filesep xlsFile.name]);
    
    %find the nextblock anotations
    [newBlockRow newBlockCol] = find(strcmp(txt,'nextblock'));
    
    %remove other annotations, apart from those in column 1
    newBlockRow(newBlockCol>1) = [];
    newBlockCol(newBlockCol>1) = [];
   
    %number of different personal stimuli
    numStimuli      = newBlockRow(2) - newBlockRow(1) -1;
    numRepetitions  = nBlocks * (newBlockRow(3) - newBlockRow(2) -1) / numStimuli;
    
    picturePairs = zeros(numStimuli, 1 + numRepetitions);
    
    frameNum = [repmat(NaN, 1, newBlockRow(1))...
        (1:numStimuli) ...
        NaN ...
        numStimuli+(1:numStimuli*numRepetitions/nBlocks) ...
        NaN ...
        numStimuli*(1+numRepetitions/nBlocks) + (1:numStimuli*numRepetitions/nBlocks)...
        ];
    
    for stim = 1:numStimuli
        
        %get trial number from block 0 (stimulus generation)
        stimNumber = num(newBlockRow(1) + stim, trialNumCol);
        
        %get the indexes of all responses in blocks 1 and 2 responding to this
        %stimulus
        [respRow respCol] = find(num(:,fromTrialCol) == stimNumber);
        respRow(respRow < newBlockRow(1)) = [];
        
        picturePairs(stim,:) = [stim frameNum(respRow)];
        
    end
    
    % picturePairs now contains one row per 8-iad of images. Each row is the
    % response to the individually-generated stimulus, which is indexed on the
    % first column.
    
    % get the frame names, this -1 correction is needed because it starts from
    % 'frame0.pts', which will correpond to index 1 in matlab
    picturePairs = picturePairs - 1;
    
    %% READ IN DATA AND DO RIGID ALIGNMENT (PROCRUSTES)
    % point_seq folder cointains only the fitted frames. points containes only
    % the manually built frames
    [points fileInfo] = read_points([rawBehaveDataDir filesep 'data_landmarksOnly' filesep subj '/point_seq/']);
    
    for expr = 1:size(picturePairs,1)
        targetImage     = picturePairs(expr,1);
        responseImages  = picturePairs(expr,2:end);
        theFrameIndex_target = find( cellfun(@length, strfind(fileInfo.name,['frame' num2str(targetImage) '.pts'])) );

        for r = 1:length(responseImages)
            theFrameIndex_response = find( cellfun(@length, strfind(fileInfo.name,['frame' num2str(responseImages(r)) '.pts'])) );
            
            %solely for reference, get the original, nonaligned faces
            originalFace{expr, r} = points(:,:,theFrameIndex_response);
            targetFace{expr}      = points(:,:,theFrameIndex_target);
            
            %align with procrustes using the 5 invariant points as
            %reference
            [alignDist(expr,r) alignedPoints{expr,r} transf] = procrustes(targetFace{expr}(invariant5points,:), ...
                                                                          originalFace{expr,r}(invariant5points,:));
            
            %transform all the points of the face based on the transformation parameters calculated
            %for the points you consider invariant
            alignedFace{expr,r} = transf.b * originalFace{expr,r} * transf.T + repmat( transf.c(1,:),length(originalFace{expr,r}),1 );

            %you could save the transformation matrix
            %allTransfs(thisFace,r) = transf;

            %store each response
            preprocessed.procrustesAligned{expr,r+1} = alignedFace{expr,r};
        end
        %store each target image
        preprocessed.procrustesAligned{expr,1} = targetFace{expr};
    end
    
    %plot and save a randomly selected expession if you like
    expr = 7;
    h = figure;
    plot(targetFace{expr}(:,1), -targetFace{expr}(:,2), 'o')
    hold on;
    for r = 1:length(responseImages)
        plot(alignedFace{expr,r}(:,1), -alignedFace{expr,r}(:,2)) %The - sign inverts the face so it's properly upside-up.
    end
    savefig(h, [preprocessedDir filesep 'preprocessed_5points' subjects{s} '.fig'])
    hold off
    
    save([preprocessedDir filesep 'preprocessed_5points' subjects{s} '.mat'])
    
end

picturePairs
preprocessed


